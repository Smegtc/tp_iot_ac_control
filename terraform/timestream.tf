# aws_timestreamwrite_database

resource "aws_timestreamwrite_database" "ttf-timestream" {
  database_name = "ttf-iot"
}

resource "aws_timestreamwrite_table" "yltp-timestream-table" {
  database_name = aws_timestreamwrite_database.ttf-timestream.database_name
  table_name    = "ttf-temperaturesensor"
}

# aws_timestreamwrite_table linked to the database
